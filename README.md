#An Android Test App for CashBack

This project contains two flavors, mock and real.

For the mock flavor, the merchant list is fetched from a local json file,
you can check the format [here](app/src/mock/assets/mock/get_merchant.json),

In order to switch to the real data set, you can use the real flavor, remember to
set the variable **API_HOST** in [build.gradle](app/build.gradle) for both debug and production build


**Requirements**

* Latest Android SDK tools
* Latest Android platform tools
* Android SDK Build tools 23.0.2
* Android SDK 23
* Android Support Repository
* Android Support Library 23.1.0
* Android DataBinding 1.0-rc4

**Build**

```
./gradlew clean assembleMockDebug
```

##Unit Test

Using [Robolectric](http://robolectric.org/) for unit testing

**There is a [known issue](https://github.com/robolectric/robolectric/issues/2143) when working with the latest android plugin for databinding**
**As a workaround we will use the old databinding plugin 1.0-rc4**

```
./gradlew clean testMockDebug
```

##UI Test

Using [Espresso](https://google.github.io/android-testing-support-library/docs/espresso/index.html) for UI Automation

**Note there is a [known issue](https://code.google.com/p/android/issues/detail?id=182715) when working with databinding**
**A workaround is [here](http://stackoverflow.com/a/28863160/1629707)**

```
./gradlew clean connectedMockDebugAndroidTest
```

##Open Source Project

* [Dagger 2](http://google.github.io/dagger/) to make all the dependency inject magic
* [Butter Knife](http://jakewharton.github.io/butterknife/) to inject view, instead of calling findViewById, install a [android studio plugin](https://github.com/inmite/android-butterknife-zelezny) to auto generate the code
* [Picasso](http://square.github.io/picasso/) as the image loader
* [Retrofit](http://square.github.io/retrofit/) as the REST client
* [RxAndroid](https://github.com/ReactiveX/RxAndroid) to do the reactive programming


##Credit

* [android-apt](https://bitbucket.org/hvisser/android-apt) to work with annotation processors in combination with Android Studio.
* [Mockaroo](https://www.mockaroo.com) to generate the mocked json data
* [Material Icon](https://www.google.com/design/icons/)
* [Material Palette](http://materialpalette.com)