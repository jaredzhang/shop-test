package com.jaredzhang.testapp;

import com.jaredzhang.testapp.data.api.TestApiModule;
import com.jaredzhang.testapp.ui.MainActivityUITests;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by jaredzhang on 24/11/15.
 */

@Singleton
@Component(modules = {
        AppModule.class,
        TestApiModule.class
}
)
public interface TestAppComponent extends AppComponent{

        void inject(MainActivityUITests mainActivityUITests);
}
