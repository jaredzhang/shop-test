package com.jaredzhang.testapp.ui;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.jaredzhang.testapp.MockClient;
import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.TestShopAppApplication;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.hamcrest.core.AllOf;
import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import javax.inject.Inject;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.jaredzhang.testapp.ui.TestUtils.isRefreshing;
import static com.jaredzhang.testapp.ui.TestUtils.withRecyclerView;
import static com.squareup.okhttp.Protocol.HTTP_1_1;
import static android.support.test.espresso.intent.Intents.intended;
import static org.hamcrest.core.AllOf.allOf;


@RunWith(AndroidJUnit4.class)
public class MainActivityUITests {

    @Inject
    MockClient mMockClient;

    TestShopAppApplication applicationContext;

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(
            MainActivity.class, true, false
    );

    @Before
    public void setUp() {
        applicationContext = (TestShopAppApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();
        applicationContext.testComponent().inject(this);
    }

    @Test
    public void checkLoadingSuccess() throws IOException {

        final String json = TestUtils.readFromAsset(InstrumentationRegistry.getContext(), "merchant.json");
        Response.Builder builder = new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).protocol(HTTP_1_1);
        mMockClient.enqueueResponse(builder);

        activityRule.launchActivity(new Intent());

        //assert the first item
        onView(withRecyclerView(R.id.recycler_view).atPositionOnView(0, R.id.tv_title))
                .check(matches(withText("name")));
        onView(withRecyclerView(R.id.recycler_view).atPositionOnView(0, R.id.tv_desc))
                .check(matches(withText("description")));
        onView(withRecyclerView(R.id.recycler_view).atPositionOnView(0, R.id.tv_cash_back))
                .check(matches(withText(applicationContext.getString(R.string.cash_back, 74))));
        onView(withRecyclerView(R.id.recycler_view).atPositionOnView(0, R.id.tv_exp))
                .check(matches(withText(applicationContext.getString(R.string.exp, "11/28/2014"))));

        onView(withId(R.id.refresh_view)).check(matches(isRefreshing(Is.is(false))));

    }

    @Test
    public void checkLoadingError() throws IOException {

        mMockClient.enqueueIOException(new IOException("time out"));
        activityRule.launchActivity(new Intent());

        onView(withRecyclerView(R.id.recycler_view).atPosition(0))
                .check(doesNotExist());
        onView(withId(R.id.refresh_view)).check(matches(isRefreshing(Is.is(false))));

    }


    @Test
    public void checkClickingItem() throws IOException {

        final String json = TestUtils.readFromAsset(InstrumentationRegistry.getContext(), "merchant.json");
        Response.Builder builder = new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).protocol(HTTP_1_1);
        mMockClient.enqueueResponse(builder);

        activityRule.launchActivity(new Intent());

        //click the first item
        onView(withRecyclerView(R.id.recycler_view).atPosition(0))
                .perform(click());

        onView(withId(R.id.tv_title))
                .check(matches(withText("name")));
        onView(withId(R.id.tv_desc))
                .check(matches(withText("description")));
        onView(withId(R.id.tv_cash_back))
                .check(matches(withText(applicationContext.getString(R.string.cash_back, 74))));
        onView(withId(R.id.tv_exp))
                .check(matches(withText(applicationContext.getString(R.string.exp, "11/28/2014"))));
    }

    // Ignore the test for further investigation
    @Test @Ignore
    public void checkClickingShop() throws IOException {
        final String json = TestUtils.readFromAsset(InstrumentationRegistry.getContext(), "merchant.json");
        Response.Builder builder = new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).protocol(HTTP_1_1);
        mMockClient.enqueueResponse(builder);

        activityRule.launchActivity(new Intent());

        onView(withId(R.id.recycler_view))
                .perform(TestUtils.actionOnItemViewAtPosition(0,
                        R.id.btn_shop,
                        click()));
    }

    // Ignore the test for further investigation
    @Test @Ignore
    public void checkClickingShare() throws IOException {
        final String json = TestUtils.readFromAsset(InstrumentationRegistry.getContext(), "merchant.json");
        Response.Builder builder = new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).protocol(HTTP_1_1);
        mMockClient.enqueueResponse(builder);

        activityRule.launchActivity(new Intent());

        onView(withId(R.id.recycler_view))
                .perform(TestUtils.actionOnItemViewAtPosition(0,
                        R.id.iv_share,
                        click()));
    }

    @After
    public void tearDown() {
    }
}
