package com.jaredzhang.testapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.jaredzhang.testapp.data.MerchantDataManager;
import com.jaredzhang.testapp.data.api.ShopAppApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jaredzhang on 23/11/15.
 */

@Module
public class AppModule {
    private final ShopAppApplication mShopAppApplication;

    public AppModule(ShopAppApplication shopAppApplication) {
        mShopAppApplication = shopAppApplication;
    }

    @Provides
    @Singleton
    public Context providesApplicationContext() {
        return mShopAppApplication;
    }

    @Provides
    @Singleton
    public SharedPreferences providesDefaultSharedPreference() {
        return PreferenceManager.getDefaultSharedPreferences(mShopAppApplication);
    }

    @Provides
    @Singleton
    public MerchantDataManager providesMerchantDataManager(Context context, ShopAppApi shopAppApi) {
        return new MerchantDataManager(context, shopAppApi);
    }
}
