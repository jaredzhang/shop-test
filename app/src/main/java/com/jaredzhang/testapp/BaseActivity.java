package com.jaredzhang.testapp;

import android.os.Bundle;
import android.view.MenuItem;

import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

/**
 * Created by jaredzhang on 7/10/15.
 */
public abstract class BaseActivity extends RxAppCompatActivity {

    public boolean isPaused;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    protected abstract void setupComponent();
}
