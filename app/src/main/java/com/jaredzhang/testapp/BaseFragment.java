package com.jaredzhang.testapp;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.view.LayoutInflater;
import android.view.View;

import com.trello.rxlifecycle.components.support.RxFragment;

/**
 * Created by jaredzhang on 23/11/15.
 */
public abstract class BaseFragment extends RxFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent();
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }

    /**
     * Inject any dependencies here
     */
    protected void setupComponent() {

    }

}
