package com.jaredzhang.testapp;

import rx.Scheduler;

/**
 * Created by jaredzhang on 25/11/15.
 */
public interface SchedulerFactory {
    Scheduler observedOn();
    Scheduler subscribedOn();
}
