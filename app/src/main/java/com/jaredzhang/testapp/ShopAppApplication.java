package com.jaredzhang.testapp;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

public class ShopAppApplication extends Application {
    public static String versionName;
    public static int versionCode;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            versionName = getPackageManager().getPackageInfo(
                    this.getPackageName(), 0).versionName;
            versionCode = getPackageManager().getPackageInfo(
                    this.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("PackageManager", "Error");
            e.printStackTrace();
        }
        initilizeInjector();
    }

    private AppComponent mAppComponent;

    private void initilizeInjector() {
        this.mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static ShopAppApplication get(Context context) {
        return ((ShopAppApplication) context.getApplicationContext());
    }

    public AppComponent component() {
        return mAppComponent;
    }

    /**
     * Visible only for testing purposes.
     */
    @VisibleForTesting
    public void setTestComponent(AppComponent appComponent) {
        mAppComponent = appComponent;
    }
}