package com.jaredzhang.testapp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaredzhang.testapp.BR;
import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.ui.BaseRecyclerListFragment;
import com.jaredzhang.testapp.ui.viewmodel.MerchantItemViewModel;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class MerchantListAdapter extends BaseRecyclerListFragment.PaginatedRecyclerAdapter<MerchantItemViewModel, MerchantListAdapter.ViewHolder> {

    Context mContext;

    public MerchantListAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_merchant, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.getBinding().setVariable(BR.merchant, getItem(position));
        holder.getBinding().executePendingBindings();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public ViewHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);
        }
        public ViewDataBinding getBinding() {
            return binding;
        }
    }
}
