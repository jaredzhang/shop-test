package com.jaredzhang.testapp.data;

import android.content.Context;

import com.jaredzhang.testapp.data.api.ObservableUtils;
import com.jaredzhang.testapp.data.api.ShopAppApi;
import com.jaredzhang.testapp.entity.Merchant;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * The Repository Manager where can fetch {@link com.jaredzhang.testapp.entity.Merchant} related business model from
 */
public class MerchantDataManager {

    private ShopAppApi mShopAppApi;
    private Context mContext;

    @Inject
    public MerchantDataManager(Context context, ShopAppApi shopAppApi) {
        this.mContext = context;
        this.mShopAppApi = shopAppApi;
    }

    /*******************/
    public Observable<List<Merchant>> getMerchant(int page, int pageSize) {
        return mShopAppApi.getMerchant(page, pageSize)
        .compose(ObservableUtils.<List<Merchant>>applyErrorHandling());
    }

}
