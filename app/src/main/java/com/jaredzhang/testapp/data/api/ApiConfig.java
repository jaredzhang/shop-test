package com.jaredzhang.testapp.data.api;

import com.jaredzhang.testapp.BuildConfig;

/**
 * Created by zhuodong on 11/9/14.
 */
public class ApiConfig {

    private ApiConfig() {
    }

    public static class EndPoint {
        public static final String MERCHANT = "/api/v1/merchent";
    }

    public static final int BATCH_SIZE = 20;
}
