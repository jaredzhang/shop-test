package com.jaredzhang.testapp.data.api;

import com.jaredzhang.testapp.SchedulerFactory;
import com.jaredzhang.testapp.data.api.response.ErrorResponse;

import java.io.IOException;

import retrofit.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class ObservableUtils {

    /**
     * Apply this to transformer for catching api error
     * @param <T>
     * @return
     */
    public static <T> Observable.Transformer<T,T> applyErrorHandling() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(final Observable<T> tObservable) {
                return tObservable.onErrorResumeNext(new Func1<Throwable, Observable<? extends T>>() {
                    @Override
                    public Observable<? extends T> call(Throwable throwable) {
                        if (throwable instanceof HttpException) {
                            try {
                                ErrorResponse errorResponse = JSONParser.parseFromInputStream(ErrorResponse.class, ((HttpException) throwable).response().errorBody().byteStream());
                                return Observable.error(new Throwable(errorResponse.getErrorMessage()));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        return Observable.error(throwable);
                    }
                });
            }
        };
    }

    /**
     * Transform the observable such that it is subscribed on worker thread, and observed on main thread
     * @param <T>
     * @param schedulerFactory
     * @return
     */

    public static <T> Observable.Transformer<T,T> applySchedulers(final SchedulerFactory schedulerFactory) {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> tObservable) {
                return tObservable.subscribeOn(schedulerFactory.subscribedOn()).observeOn(schedulerFactory.observedOn());
            }
        };
    }

}
