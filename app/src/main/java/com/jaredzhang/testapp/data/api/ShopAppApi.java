package com.jaredzhang.testapp.data.api;

import com.jaredzhang.testapp.entity.Merchant;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by jaredzhang on 23/11/15.
 */
public interface ShopAppApi {
    @GET(ApiConfig.EndPoint.MERCHANT)
    Observable<List<Merchant>> getMerchant(@Query("page") int page, @Query("size") int pageSize);
}
