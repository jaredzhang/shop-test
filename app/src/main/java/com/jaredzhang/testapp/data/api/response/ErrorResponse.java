package com.jaredzhang.testapp.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class ErrorResponse {

    @SerializedName("error")
    @Expose
    private Error error;

    /**
     * @return The error
     */
    public Error getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(Error error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return error.message;
    }

    public class Error {

        @SerializedName("code")
        @Expose
        private int code;
        @SerializedName("message")
        @Expose
        private String message;

        /**
         * @return The code
         */
        public int getCode() {
            return code;
        }

        /**
         * @param code The code
         */
        public void setCode(int code) {
            this.code = code;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

    }
}
