package com.jaredzhang.testapp.entity;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class Merchant {
    String name;
    String description;
    int cashback;
    String logoUrl;
    // MM/dd/yyyy
    String expiryDate;
    String category;
    String categoryLogo;
    String merchantUrl;
    String merchantDomain;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getCashback() {
        return cashback;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getCategory() {
        return category;
    }

    public String getCategoryLogo() {
        return categoryLogo;
    }

    public String getMerchantUrl() {
        return merchantUrl;
    }

    public String getMerchantDomain() {
        return merchantDomain;
    }
}
