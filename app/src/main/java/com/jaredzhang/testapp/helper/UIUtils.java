package com.jaredzhang.testapp.helper;

import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.jaredzhang.testapp.R;

/**
 *
 * UI related Utility Function
 * Created by jaredzhang on 23/11/15.
 */
public class UIUtils {

    public static void replaceFragment(FragmentManager fragmentManager, int containerResId, boolean newStack, Fragment f) {
        if (newStack) {
            for (int i = fragmentManager.getBackStackEntryCount(); i > 0; i--) {
                fragmentManager.popBackStack();
            }
        }
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, 0, android.R.anim.fade_in, 0);
        transaction.replace(containerResId, f, ((Object) f).getClass().getName());
        if (!newStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void showSnackbar(View anchorView, @StringRes int resId) {
        if (anchorView == null) {
            return;
        }

        Snackbar snackbar = Snackbar.make(anchorView, resId, Snackbar.LENGTH_LONG)
                .setActionTextColor(ContextCompat.getColor(anchorView.getContext(), R.color.accent_orange));
        snackbar.show();
    }
}
