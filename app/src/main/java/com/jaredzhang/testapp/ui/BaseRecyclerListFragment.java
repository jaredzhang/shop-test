package com.jaredzhang.testapp.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaredzhang.testapp.BaseFragment;
import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.data.api.ApiConfig;
import com.jaredzhang.testapp.helper.UIUtils;
import com.jaredzhang.testapp.ui.view.BaseListView;
import com.jaredzhang.testapp.widget.ListSwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Paginated fragment with RecyclerView
 */
public abstract class BaseRecyclerListFragment<T extends Parcelable> extends BaseFragment implements BaseListView<T> {
    protected RecyclerView mRecyclerView;
    private FooterAdapter mFooterAdapter;
    protected PaginatedRecyclerAdapter<T, ? extends RecyclerView.ViewHolder> mAdapter;
    private LoadMoreDelegate mPaginationHelper;
    private RecyclerView.OnScrollListener mLoadMoreListener;
    protected ListSwipeRefreshLayout mRefreshLayout;

    private final RecyclerView.AdapterDataObserver mDataSetObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            mRefreshLayout.hideRefreshing();
            mFooterAdapter.setHasMoreItems(false);
            mPaginationHelper.updateTotalPage(mAdapter.mTotalPages);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(getLayoutResource(), container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);


        mRefreshLayout = (ListSwipeRefreshLayout)
                view.findViewById(R.id.refresh_view);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mPaginationHelper = new LoadMoreDelegate(1);
        mPaginationHelper.attachCallback(new LoadMoreCallbacks() {
            @Override
            public void loadMore(int pageTobeLoaded) {
                mFooterAdapter.setHasMoreItems(true);
                fetchData(pageTobeLoaded);
            }
        });
        mPaginationHelper.restoreState(savedInstanceState);
        mLoadMoreListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    mPaginationHelper.handleScroll(layoutManager.findFirstVisibleItemPosition(), layoutManager.getChildCount(), layoutManager.getItemCount());
                }
            }
        };
        mRecyclerView.addOnScrollListener(mLoadMoreListener);
        mAdapter = createAdapter();
        mAdapter.registerAdapterDataObserver(mDataSetObserver);
        mAdapter.restoreState(savedInstanceState);
        mFooterAdapter = new
                FooterAdapter(mAdapter);
        mRecyclerView.setAdapter(mFooterAdapter);

        // set the app bar scroll behavior
        setContentScrollBehavior();
        return view;
    }

    private void setContentScrollBehavior() {
        View content = getActivity().findViewById(R.id.content_frame);
        if (content != null) {
            CoordinatorLayout.LayoutParams params =
                    (CoordinatorLayout.LayoutParams) content.getLayoutParams();
            params.setBehavior(new AppBarLayout.ScrollingViewBehavior());
        }
    }

    private void resetContentScrollBehavior() {
        View content = getActivity().findViewById(R.id.content_frame);
        AppBarLayout appbar = (AppBarLayout) getActivity().findViewById(R.id.appbar);
        appbar.setExpanded(true, true);
        CoordinatorLayout.LayoutParams params2 =
                (CoordinatorLayout.LayoutParams) content.getLayoutParams();
        params2.setBehavior(null);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!mAdapter.mPopulatedData) {
            mRefreshLayout.showRefreshing();
            fetchData(1);
        } else {
            notifyUIChange();
        }
    }

    /**
     * To be called after data has been fetched
     */
    protected abstract void notifyUIChange();

    @Override
    public void onDestroyView() {
        mPaginationHelper.detachCallback();
        mAdapter.unregisterAdapterDataObserver(mDataSetObserver);
        super.onDestroyView();
        resetContentScrollBehavior();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPaginationHelper.saveState(outState);
        mAdapter.saveState(outState);
    }

    @Override
    public void onDetach() {
        mRecyclerView.removeOnScrollListener(mLoadMoreListener);
        super.onDetach();
    }

    /**
     * Invalidate and refetch list view data
     */
    public void refresh() {
        mRefreshLayout.showRefreshing();
        mPaginationHelper.reset();
        mFooterAdapter.setHasMoreItems(false);
        mAdapter.invalidate();
        fetchData(1);
    }

    /**
     * Get layout resource ID for list header if any
     *
     * @return header layout resource ID or 0 if no header
     */
    @LayoutRes
    protected abstract int getLayoutResource();

    /**
     * Construct an instance of paginated list view adapter
     *
     * @return paginated adapter
     */
    protected abstract PaginatedRecyclerAdapter<T, ? extends RecyclerView.ViewHolder> createAdapter();

    /**
     * Retrieve the data for given page
     *
     * @param page the page to load
     */
    protected abstract void fetchData(int page);

    /**
     * Paginated list adapter
     *
     * @param <T> list item type
     */
    public static abstract class PaginatedRecyclerAdapter<T extends Parcelable, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
        private static final String STATE_TOTAL_PAGES = "STATE_TOTAL_PAGES";
        private static final String STATE_TOTAL_ITEMS = "STATE_TOTAL_ITEMS";
        private static final String STATE_VALID = "STATE_VALID";
        private static final String STATE_ITEMS = "STATE_ITEMS";
        private static final String STATE_POPULATE_DATA = "STATE_POPULATE_DATA";
        private int mTotalPages = Integer.MAX_VALUE;
        private boolean mValid = true;
        protected List<T> items;
        private boolean mPopulatedData;

        @Override
        public int getItemCount() {
            if (items == null) {
                return 0;
            }
            return items.size();
        }

        public void updateTotalPage(int pageNo) {
            mTotalPages = pageNo;
        }

        /**
         * Populating or appending adapter
         */
        public void populate(@Nullable List<T> items) {
            mPopulatedData = true;
            if (!mValid) {
                clear();
                mValid = true;
            }
            addItems(items);
        }

        /**
         * clear the internal item, not notifying the adapter
         */
        public void clear() {
            if (items != null) {
                items.clear();
                items = null;
            }
        }

        /**
         * Mark data set as invalid, to be cleared at next population
         */
        protected void invalidate() {
            mValid = false;
            mTotalPages = Integer.MAX_VALUE;
        }

        /**
         * Add a collection of items to the adapter
         *
         * @param items a list of items
         */
        public void addItems(List<T> items) {
            if (this.items == null) {
                this.items = new ArrayList<>();
            }
            this.items.addAll(items);
            notifyDataSetChanged();
        }

        /**
         * Get the item for the specific position
         *
         * @param position
         * @return item
         */
        public T getItem(int position) {
            if (items == null || items.size() < position) {
                return null;
            }
            return items.get(position);
        }

        /**
         * remove the item from list
         *
         * @param item the removed item
         * @return
         */
        public T remove(T item) {
            if (items != null) {
                items.remove(item);
            }
            notifyDataSetChanged();
            return item;
        }

        /**
         * check whether the list is empty
         *
         * @return
         */
        public boolean isEmpty() {
            return items == null || items.isEmpty();
        }

        /**
         * save any internal state
         *
         * @param outState
         */
        protected void saveState(Bundle outState) {
            if (outState != null) {
                outState.putInt(STATE_TOTAL_PAGES, mTotalPages);
                outState.putBoolean(STATE_VALID, mValid);
                outState.putBoolean(STATE_POPULATE_DATA, mPopulatedData);
                if (items != null) {
                    outState.putParcelableArrayList(STATE_ITEMS, new ArrayList<>(items));
                }
            }
        }

        /**
         * restore the adapter from {@link Bundle}
         *
         * @param savedInstanceState
         */
        protected void restoreState(Bundle savedInstanceState) {
            if (savedInstanceState != null) {
                mTotalPages = savedInstanceState.getInt(STATE_TOTAL_PAGES);
                mValid = savedInstanceState.getBoolean(STATE_VALID);
                mPopulatedData = savedInstanceState.getBoolean(STATE_POPULATE_DATA);
                items = savedInstanceState.getParcelableArrayList(STATE_ITEMS);
            }
        }

    }

    private static class FooterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private static final int TYPE_FOOTER = 0;
        private static final int TYPE_ITEM = 1;
        PaginatedRecyclerAdapter baseAdapter;
        boolean hasMoreItems;

        public FooterAdapter(PaginatedRecyclerAdapter baseAdapter) {
            this.baseAdapter = baseAdapter;
        }

        public void setHasMoreItems(boolean hasMoreItems) {
            this.hasMoreItems = hasMoreItems;
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == TYPE_FOOTER) {
                //inflate your layout and pass it to view holder
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_loading, parent, false);
                return new VHFooter(view);
            } else {
                return baseAdapter.onCreateViewHolder(parent, viewType);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder.getItemViewType() != TYPE_FOOTER) {
                baseAdapter.onBindViewHolder(holder, position);
            }
        }

        @Override
        public int getItemCount() {
            return baseAdapter.getItemCount() + (hasMoreItems ? 1 : 0);
        }

        @Override
        public int getItemViewType(int position) {
            if (hasMoreItems && isFooter(position)) {
                return TYPE_FOOTER;
            }
            return TYPE_ITEM;
        }

        private boolean isFooter(int position) {
            return position == getItemCount() - 1;
        }

        class VHFooter extends RecyclerView.ViewHolder {
            public VHFooter(View itemView) {
                super(itemView);
            }
        }
    }

    public void onResponseReceived(boolean success, List<T> viewModelList) {
        if (getActivity() == null) {
            return;
        }

        if (!success) {
            mRefreshLayout.setRefreshing(false);
            UIUtils.showSnackbar(mRecyclerView, R.string.error_default);
            return;
        }

        if (viewModelList != null && viewModelList.size() < ApiConfig.BATCH_SIZE) {
            mAdapter.updateTotalPage(mPaginationHelper.getCurrentPage());
        }
        mAdapter.populate(viewModelList);
        if (mPaginationHelper.getCurrentPage() == 1) {
            notifyUIChange();
        }
    }
}
