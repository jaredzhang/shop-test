package com.jaredzhang.testapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaredzhang.testapp.BaseFragment;
import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.helper.LogUtil;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class FeaturedFragment extends BaseFragment {

    private View appBarAddition;
    private AppBarLayout appBar;

    public static final String TAG = LogUtil.makeLogTag(FeaturedFragment.class);

    @Bind(R.id.view_pager)
    ViewPager mViewPager;

    private TabLayout mTabLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_featured, container, false);
        ButterKnife.bind(this, view);

        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                LogUtil.LOGD(TAG, " get item " + position);
                if (position == 0) {
                    return Fragment.instantiate(getContext(), HotDealsFragment.class.getName());
                }
                return new CommingSoonFragment();
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                LogUtil.LOGD(TAG, " destroy item " + position);

                super.destroyItem(container, position, object);
            }

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return getResources().getStringArray(R.array.feature_tab_title)[position];
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appBar = (AppBarLayout) getActivity().findViewById(R.id.appbar);
        appBarAddition = LayoutInflater.from(getContext()).inflate(R.layout.layout_tab, appBar , false);
        appBar.addView(appBarAddition);
        customizeAppBar(appBar);

        mTabLayout = (TabLayout) appBar.findViewById(R.id.tab_layout);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void customizeAppBar(AppBarLayout appBar) {
        Toolbar toolbar = (Toolbar) appBar.findViewById(R.id.toolbar);
        AppBarLayout.LayoutParams params =
                (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS | AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        appBar.removeView(appBarAddition);
        appBarAddition = null;
        mTabLayout = null;
        ButterKnife.unbind(this);
    }
}
