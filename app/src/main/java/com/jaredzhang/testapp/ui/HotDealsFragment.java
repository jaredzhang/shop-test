package com.jaredzhang.testapp.ui;

import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.SchedulerFactory;
import com.jaredzhang.testapp.ShopAppApplication;
import com.jaredzhang.testapp.adapter.MerchantListAdapter;
import com.jaredzhang.testapp.data.api.ObservableUtils;
import com.jaredzhang.testapp.entity.Merchant;
import com.jaredzhang.testapp.helper.LogUtil;
import com.jaredzhang.testapp.ui.module.HotDealsFragmentModule;
import com.jaredzhang.testapp.ui.view.HotDealsView;
import com.jaredzhang.testapp.ui.viewmodel.HotDealsViewModel;
import com.jaredzhang.testapp.ui.viewmodel.MerchantItemViewModel;
import com.trello.rxlifecycle.FragmentEvent;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Action1;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class HotDealsFragment extends BaseRecyclerListFragment<MerchantItemViewModel> implements HotDealsView {
    public static final String TAG = LogUtil.makeLogTag(HotDealsFragment.class);

    @Inject
    HotDealsViewModel mViewModel;

    @Inject
    SchedulerFactory mSchedulerFactory;

    @Override
    protected void notifyUIChange() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_hot_deals;
    }

    @Override
    protected PaginatedRecyclerAdapter<MerchantItemViewModel, MerchantListAdapter.ViewHolder> createAdapter() {
        return new MerchantListAdapter(getContext());
    }

    @Override
    protected void fetchData(int page) {
        mViewModel.fetchData(page)
                .compose(ObservableUtils.<List<MerchantItemViewModel>>applySchedulers(mSchedulerFactory))
                .compose(this.<List<MerchantItemViewModel>>bindUntilEvent(FragmentEvent.DETACH))
                .subscribe(new Action1<List<MerchantItemViewModel>>() {
                    @Override
                    public void call(List<MerchantItemViewModel> viewModelList) {
                        onResponseReceived(true, viewModelList);
                        LogUtil.LOGD(TAG, "get merchant response");
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        onResponseReceived(false, null);
                        LogUtil.LOGD(TAG, "error");
                    }
                });
    }

    @Override
    protected void setupComponent() {
        ShopAppApplication.get(getContext()).component().plus(new HotDealsFragmentModule(this)).inject(this);
    }
}
