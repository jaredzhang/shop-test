package com.jaredzhang.testapp.ui;

/**
 * Callback interface to notify listener to load more
 */
public interface LoadMoreCallbacks {
    /**
     * Fired when listener should load more
     *
     * @param pageTobeLoaded page to be loaded
     */
    void loadMore(int pageTobeLoaded);
}
