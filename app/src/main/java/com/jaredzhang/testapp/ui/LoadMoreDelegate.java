package com.jaredzhang.testapp.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Helper class to perform load more action in any list of items. It helps to
 * detect whether users scrolling action exceeds some threshold, it will notify
 * the registered party to perform mLoading more items.
 *
 */
public class LoadMoreDelegate {
    private static final String STATE_TOTAL_PAGES = "STATE_TOTAL_PAGES";
    private static final String STATE_PREVIOUS_TOTAL = "STATE_PREVIOUS_TOTAL";
    private static final String STATE_PAGE_TO_LOAD = "STATE_PAGE_TO_LOAD";
    private static final String STATE_LOADING = "STATE_LOADING";
    private static final String STATE_CURRENT_PAGE = "STATE_CURRENT_PAGE";
    private LoadMoreCallbacks mCallbacks;
    private int mTotalPage = Integer.MAX_VALUE;
    private int mVisibleThreshold = 3;
    private int mPreviousTotal = 0;
    private int mPageTobeLoaded = 1;
    private int mCurrentPage = 1;
    private boolean mLoading = true;

    /**
     * Create an instance of {@link LoadMoreDelegate}
     *
     * @param visibleThreshold number of items left to scroll before it should load in advance
     */
    public LoadMoreDelegate(int visibleThreshold) {
        mVisibleThreshold = visibleThreshold;
    }

    /**
     * attach {@link LoadMoreCallbacks}
     * @param callbacks callback to receive notification when it should load more
     */
    public void attachCallback(@NonNull LoadMoreCallbacks callbacks) {
        mCallbacks = callbacks;
    }

    /**
     * attach {@link LoadMoreCallbacks}
     */
    public void detachCallback() {
        mCallbacks = null;
    }

    /**
     * Reset listener to its original state
     */
    public void reset() {
        mTotalPage = Integer.MAX_VALUE;
        mPreviousTotal = 0;
        mPageTobeLoaded = 1;
        mLoading = true;
        mCurrentPage = 1;
    }

    /**
     * Set total number of pages so that the listener will stop listening upon reaching the last page.
     *
     * @param totalPage total number of pages, or -1 for infinite scrolling
     */
    public void updateTotalPage(int totalPage) {
        mTotalPage = totalPage;
    }

    /**
     * get the current page
     * @return
     */
    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void handleScroll(int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mLoading) {
            if (totalItemCount > mPreviousTotal) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
                mPageTobeLoaded++;
            }
        } else {
            if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + mVisibleThreshold) &&
                    mTotalPage > 0 && mPageTobeLoaded <= mTotalPage) {
                mPreviousTotal = totalItemCount;
                mLoading = true;
                mCurrentPage = mPageTobeLoaded;
                if (mCallbacks != null) {
                    mCallbacks.loadMore(mPageTobeLoaded);
                }
            }
        }
    }

    /**
     * save any internal state
     * @param outState
     */
    public void saveState(Bundle outState) {
        if (outState != null) {
            outState.putInt(STATE_TOTAL_PAGES, mTotalPage);
            outState.putInt(STATE_PREVIOUS_TOTAL, mPreviousTotal);
            outState.putInt(STATE_PAGE_TO_LOAD, mPageTobeLoaded);
            outState.putInt(STATE_CURRENT_PAGE, mCurrentPage);
            outState.putBoolean(STATE_LOADING, mLoading);
        }
    }

    /**
     * restore the internal state
     * @param savedInstanceState
     */
    public void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mTotalPage = savedInstanceState.getInt(STATE_TOTAL_PAGES);
            mPreviousTotal = savedInstanceState.getInt(STATE_PREVIOUS_TOTAL);
            mPageTobeLoaded = savedInstanceState.getInt(STATE_PAGE_TO_LOAD);
            mCurrentPage = savedInstanceState.getInt(STATE_CURRENT_PAGE);
            mLoading = savedInstanceState.getBoolean(STATE_LOADING);
        }
    }
}
