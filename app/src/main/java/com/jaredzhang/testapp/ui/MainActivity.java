package com.jaredzhang.testapp.ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.jaredzhang.testapp.BaseActivity;
import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.ShopAppApplication;
import com.jaredzhang.testapp.data.MerchantDataManager;
import com.jaredzhang.testapp.helper.LogUtil;
import com.jaredzhang.testapp.helper.UIUtils;
import com.jaredzhang.testapp.ui.module.MainActivityModule;
import com.jaredzhang.testapp.ui.view.MainView;
import com.trello.rxlifecycle.ActivityEvent;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainView {

    private static final long DRAWER_SLIDE_DURATION_MS = 250;
    private static final String SELECTED_ITEM_ID = "selected_id";
    private static final String TAG = LogUtil.makeLogTag(MainActivity.class.getClass());
    @Bind(R.id.nav_view)
    NavigationView mNavView;
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle mDrawerToggle;
    private int mSelectedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(isDrawerIndicatorEnabled());
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isDrawerIndicatorEnabled()) {
                    onBackPressed();
                }
            }
        });
        mNavView.setNavigationItemSelectedListener(this);
        if (savedInstanceState == null) {
            openRootFragment();
        } else {
            mSelectedId = savedInstanceState.getInt(SELECTED_ITEM_ID);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void openRootFragment() {
        mSelectedId = R.id.nav_featured;
        mNavView.setCheckedItem(R.id.nav_featured);
        openFragment(FeaturedFragment.class, true, null);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (isDrawerIndicatorEnabled()) {
            mDrawerToggle.syncState();
            findViewById(R.id.content_frame)
                    .setContentDescription(getString(R.string.action_bar_home_description));
        } else {
            findViewById(R.id.content_frame)
                    .setContentDescription(getString(R.string.action_bar_up_description));
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (shouldCreateRootFragment()) {
                openRootFragment();
            } else {
                super.onBackPressed();
            }
            mDrawerToggle.setDrawerIndicatorEnabled(isDrawerIndicatorEnabled());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED_ITEM_ID, mSelectedId);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            toggleDrawer();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    private void toggleDrawer() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private boolean shouldCreateRootFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            return false;
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(getContentView().getId());
        return !(fragment instanceof FeaturedFragment);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == mSelectedId) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }

        if (id == R.id.nav_featured) {
            openFragment(FeaturedFragment.class, true, null);
        } else {
            openFragment(CommingSoonFragment.class, true, null);
        }
        mSelectedId = id;

        return true;
    }


    /**
     * Create fragment and replace into activity container. Fragment will be tagged with its class name.
     *
     * @param fragmentClass fragment class
     * @param arguments     fragment arguments or null
     * @param <T>           fragment class type
     * @return added fragment
     */
    public  <T extends Fragment> T openFragment(Class<T> fragmentClass, final boolean newStack,
                                                  @Nullable Bundle arguments) {

        final T f = (T) Fragment.instantiate(this, fragmentClass.getName(), arguments);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            mDrawerLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    replaceContentFragment(newStack, f);
                }
            }, DRAWER_SLIDE_DURATION_MS);
        } else {
            replaceContentFragment(newStack, f);
        }
        return f;
    }

    private <T extends Fragment> void replaceContentFragment(boolean newStack, T f) {
        mDrawerToggle.setDrawerIndicatorEnabled(newStack);
        UIUtils.replaceFragment(getSupportFragmentManager(), getContentView().getId(), newStack, f);
    }

    /**
     * Get reference to view group that hosts content
     *
     * @return content view
     */
    public ViewGroup getContentView() {
        return (ViewGroup) findViewById(R.id.content_frame);
    }

    @Override
    protected void setupComponent() {
        ShopAppApplication.get(this).component().plus(new MainActivityModule(this)).inject(this);
    }

    /**
     * Method which controls the display of the drawer indicator
     * @return true if you want the drawer indicator to be displayed
     *  return false if you want the home as up indicator and move one level up to be displayed
     */
    protected boolean isDrawerIndicatorEnabled() {
        return getSupportFragmentManager().getBackStackEntryCount() == 0;
    }
}

