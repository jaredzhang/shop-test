package com.jaredzhang.testapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.jaredzhang.testapp.BaseFragment;
import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.databinding.FragmentMerchantDetailBinding;
import com.jaredzhang.testapp.helper.LogUtil;
import com.jaredzhang.testapp.ui.viewmodel.MerchantItemViewModel;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class MerchantDetailFragment extends BaseFragment {

    public static final String TAG = LogUtil.makeLogTag(MerchantDetailFragment.class);
    public static String EXTRA_MERCHANT = TAG+"_EXTRA_MERCHANT";

    private MerchantItemViewModel mItemViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItemViewModel = getArguments().getParcelable(EXTRA_MERCHANT);
        }
        if (mItemViewModel == null) {
            throw new IllegalArgumentException("the merchant item can not be empty");
        }
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_merchant_detail, container, false);
        FragmentMerchantDetailBinding binding = FragmentMerchantDetailBinding.bind(view);
        binding.setMerchant(mItemViewModel);
        getActivity().setTitle(mItemViewModel.getTitle());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null) {
            getActivity().setTitle(getString(R.string.app_name));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_share, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_share) {
            mItemViewModel.onShareClick(getView());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
