package com.jaredzhang.testapp.ui.component;

import com.jaredzhang.testapp.ActivityScope;
import com.jaredzhang.testapp.ui.MainActivity;
import com.jaredzhang.testapp.ui.module.MainActivityModule;

import dagger.Subcomponent;

/**
 * Created by jaredzhang on 23/11/15.
 */

@ActivityScope
@Subcomponent(modules = {
                MainActivityModule.class,
                 }
          )
public interface MainActivityComponent {
    void inject(MainActivity activity);
}
