package com.jaredzhang.testapp.ui.module;


import android.content.Context;

import com.jaredzhang.testapp.ActivityScope;
import com.jaredzhang.testapp.data.MerchantDataManager;
import com.jaredzhang.testapp.ui.view.HotDealsView;
import com.jaredzhang.testapp.ui.view.MainView;
import com.jaredzhang.testapp.ui.viewmodel.HotDealsViewModel;
import com.jaredzhang.testapp.ui.viewmodel.MainViewModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jaredzhang on 18/11/15.
 */
@Module
public class HotDealsFragmentModule {
    private HotDealsView mHotDealsView;

    public HotDealsFragmentModule(HotDealsView hotDealsView) {
        mHotDealsView = hotDealsView;
    }

    @Provides
    @ActivityScope
    HotDealsView provideHotDealsView() {
        return mHotDealsView;
    }

    @Provides
    @ActivityScope
    HotDealsViewModel provideHotDealsViewModel(Context context, MerchantDataManager merchantDataManager, HotDealsView hotDealsView) {
        return new HotDealsViewModel(context, merchantDataManager, hotDealsView);
    }

}
