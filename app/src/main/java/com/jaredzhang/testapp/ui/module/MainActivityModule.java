package com.jaredzhang.testapp.ui.module;


import com.jaredzhang.testapp.ActivityScope;
import com.jaredzhang.testapp.ui.view.MainView;
import com.jaredzhang.testapp.ui.viewmodel.MainViewModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jaredzhang on 18/11/15.
 */
@Module
public class MainActivityModule {
    private MainView mMainView;

    public MainActivityModule(MainView mainView) {
        mMainView = mainView;
    }

    @Provides
    @ActivityScope
    MainView provideSplashView() {
        return mMainView;
    }

    @Provides
    @ActivityScope
    MainViewModel provideMainViewModel(MainViewModel mainViewModel) {
        return mainViewModel;
    }

}
