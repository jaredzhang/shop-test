package com.jaredzhang.testapp.ui.view;

import com.trello.rxlifecycle.ActivityEvent;

import rx.Observable;

/**
 * Created by jaredzhang on 18/11/15.
 */
public interface BaseView {
    <T> Observable.Transformer<T,T> bindUntilEvent();
}
