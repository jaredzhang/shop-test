package com.jaredzhang.testapp.ui.view;

import com.jaredzhang.testapp.ui.viewmodel.MerchantItemViewModel;

/**
 * Created by jaredzhang on 23/11/15.
 */
public interface HotDealsView extends BaseListView<MerchantItemViewModel> {
}
