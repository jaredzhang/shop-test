package com.jaredzhang.testapp.ui.viewmodel;

import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.jaredzhang.testapp.R;
import com.squareup.picasso.Picasso;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class CustomAdapter {
    @BindingAdapter("app:imageUrl")
     public static void loadImage(ImageView view, String url) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(view.getContext()).load(url).placeholder(R.mipmap.ic_launcher).into(view);
        } else {
            view.setImageResource(R.mipmap.ic_launcher);
        }
    }

//    @BindingConversion
//    public static String convertBindableToString(ObservableField<String> bindableString) {
//        return bindableString.get();
//    }
//
//    @BindingAdapter("app:binding")
//    public static void setText(EditText view, final ObservableField<String> bindableString) {
//        if (view.getTag(R.id.binded) == null) {
//            view.setTag(R.id.binded, true);
//            view.addTextChangedListener(new TextWatcherAdapter() {
//                @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    bindableString.set(s.toString());
//                }
//            });
//        }
//        String newValue = bindableString.get();
//        if (view.getText() == null || !view.getText().toString().equals(newValue)) {
//            view.setText(newValue);
//        }
//    }
}
