package com.jaredzhang.testapp.ui.viewmodel;

import android.content.Context;

import com.jaredzhang.testapp.data.MerchantDataManager;
import com.jaredzhang.testapp.data.api.ApiConfig;
import com.jaredzhang.testapp.data.api.ObservableUtils;
import com.jaredzhang.testapp.entity.Merchant;
import com.jaredzhang.testapp.helper.LogUtil;
import com.jaredzhang.testapp.ui.view.BaseListView;
import com.jaredzhang.testapp.ui.view.HotDealsView;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class HotDealsViewModel {
    public static final String TAG = LogUtil.makeLogTag(HotDealsViewModel.class);

    MerchantDataManager mMerchantDataManager;
    HotDealsView mHotDealsView;
    Context mContext;

    public HotDealsViewModel(Context context, MerchantDataManager merchantDataManager, HotDealsView hotDealsView) {
        mMerchantDataManager = merchantDataManager;
        mHotDealsView = hotDealsView;
        mContext = context;
    }

    public Observable<List<MerchantItemViewModel>> fetchData(int page) {
        return mMerchantDataManager.getMerchant(page, ApiConfig.BATCH_SIZE)
                .flatMap(new Func1<List<Merchant>, Observable<Merchant>>() {
                    @Override
                    public Observable<Merchant> call(List<Merchant> merchants) {
                        return Observable.from(merchants);
                    }
                })
                .map(new Func1<Merchant, MerchantItemViewModel>() {
                    @Override
                    public MerchantItemViewModel call(Merchant merchant) {
                        return MerchantItemViewModel.mapFrom(mContext, merchant);
                    }
                })
                .toList();
    }
}
