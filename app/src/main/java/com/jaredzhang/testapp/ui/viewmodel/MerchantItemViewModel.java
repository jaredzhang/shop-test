package com.jaredzhang.testapp.ui.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;

import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.entity.Merchant;
import com.jaredzhang.testapp.helper.Utils;
import com.jaredzhang.testapp.ui.MainActivity;
import com.jaredzhang.testapp.ui.MerchantDetailFragment;

/**
 * Created by jaredzhang on 23/11/15.
 */
public class MerchantItemViewModel extends BaseObservable implements Parcelable{

    String logoUrl;
    String categoryLogoUrl;
    String title;
    String description;
    String expiryDate;
    String cashBack;
    String merchantUrl;
    String merchantDomain;


    @Bindable
    public String getLogoUrl() {
        return logoUrl;
    }

    @Bindable
    public String getCategoryLogoUrl() {
        return categoryLogoUrl;
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    @Bindable
    public String getExpiryDate() {
        return expiryDate;
    }

    @Bindable
    public String getCashBack() {
        return cashBack;
    }

    @Bindable
    public String getMerchantUrl() {
        return merchantUrl;
    }

    public static MerchantItemViewModel mapFrom(Context context, Merchant merchant) {
        MerchantItemViewModel viewModel = new MerchantItemViewModel();
        viewModel.categoryLogoUrl = merchant.getCategoryLogo();
        viewModel.title = merchant.getName();
        viewModel.description = merchant.getDescription();
        viewModel.cashBack = merchant.getCashback() == 0? "":context.getString(R.string.cash_back, merchant.getCashback());
        viewModel.expiryDate = TextUtils.isEmpty(merchant.getExpiryDate()) ? context.getString(R.string.never_expires) : context.getString(R.string.exp, merchant.getExpiryDate());
        viewModel.merchantUrl = merchant.getMerchantUrl();
        viewModel.logoUrl = merchant.getLogoUrl();
        viewModel.merchantDomain = merchant.getMerchantDomain();
        return viewModel;
    }


    public MerchantItemViewModel() {
    }

    public void onShareClick(View view) {
        Utils.shareLink(view.getContext(), merchantUrl);
    }

    public void onShopClick(View view) {
        Utils.viewLink(view.getContext(), merchantUrl);
    }

    public void onItemClick(View view) {
        if (view.getContext() instanceof MainActivity) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(MerchantDetailFragment.EXTRA_MERCHANT, this);
            ((MainActivity) view.getContext()).openFragment(MerchantDetailFragment.class, false, bundle);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.logoUrl);
        dest.writeString(this.categoryLogoUrl);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.expiryDate);
        dest.writeString(this.cashBack);
        dest.writeString(this.merchantUrl);
        dest.writeString(this.merchantDomain);
    }

    protected MerchantItemViewModel(Parcel in) {
        this.logoUrl = in.readString();
        this.categoryLogoUrl = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.expiryDate = in.readString();
        this.cashBack = in.readString();
        this.merchantUrl = in.readString();
        this.merchantDomain = in.readString();
    }

    public static final Creator<MerchantItemViewModel> CREATOR = new Creator<MerchantItemViewModel>() {
        public MerchantItemViewModel createFromParcel(Parcel source) {
            return new MerchantItemViewModel(source);
        }

        public MerchantItemViewModel[] newArray(int size) {
            return new MerchantItemViewModel[size];
        }
    };
}
