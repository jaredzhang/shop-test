package com.jaredzhang.testapp.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

import com.jaredzhang.testapp.R;


/**
 *  Minor extension to {@link android.support.v4.widget.SwipeRefreshLayout} that allows toggling
 *  refresh indicator
 */
public class ListSwipeRefreshLayout extends SwipeRefreshLayout {

    public ListSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setColorSchemeResources(R.color.accent);
    }

    /**
     * Start animation showing refresh indicator
     */
    public void showRefreshing() {
        if (isRefreshing()) {
            return;
        }
        post(new Runnable() {
            @Override
            public void run() {
                setRefreshing(true);
            }
        });
    }

    /**
     * Hide refresh indicator
     */
    public void hideRefreshing() {
        post(new Runnable() {
            @Override
            public void run() {
                setRefreshing(false);
            }
        });
    }
}
