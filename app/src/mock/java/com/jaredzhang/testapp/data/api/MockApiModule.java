package com.jaredzhang.testapp.data.api;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.jaredzhang.testapp.SchedulerFactory;
import com.jaredzhang.testapp.ShopAppApplication;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jaredzhang on 16/11/15.
 */

@Module
public class MockApiModule extends BaseApiModule {

    @Provides
    @Singleton
    public Interceptor providesInterceptor(Context context) {
        return new MockHttpClient(context);
    }

    @Provides
    @Singleton
    public SchedulerFactory providesSchedulers() {
        return new SchedulerFactory() {
            @Override
            public Scheduler observedOn() {
                return AndroidSchedulers.mainThread();
            }

            @Override
            public Scheduler subscribedOn() {
                return Schedulers.io();
            }
        };
    }
}
