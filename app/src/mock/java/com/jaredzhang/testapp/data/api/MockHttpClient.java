package com.jaredzhang.testapp.data.api;

import android.content.Context;

import com.jaredzhang.testapp.ShopAppApplication;
import com.jaredzhang.testapp.helper.Utils;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import javax.inject.Inject;


import static com.squareup.okhttp.Protocol.HTTP_1_1;


/**
 * Created by jaredzhang on 23/11/15.
 */
final class MockHttpClient implements Interceptor {
    private Context mContext;
    private static final String PATH_PREFIX = "mock";

    @Inject
    public MockHttpClient(Context context) {
        mContext = context;
    }

    @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        String fileName = null;
        if (ApiConfig.EndPoint.MERCHANT.contains(request.url().getPath())) {
            fileName = "get_merchant.json";
        }
        final String json = Utils.readFromAsset(mContext, PATH_PREFIX + "/" + fileName);
        return new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).request(request).protocol(HTTP_1_1).build();
    }
}
