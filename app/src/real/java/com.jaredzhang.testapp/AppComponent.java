package com.jaredzhang.testapp;

import com.jaredzhang.testapp.data.api.RealApiModule;
import com.jaredzhang.testapp.ui.MainActivity;
import com.jaredzhang.testapp.ui.component.HotDealsFragmentComponent;
import com.jaredzhang.testapp.ui.component.MainActivityComponent;
import com.jaredzhang.testapp.ui.module.HotDealsFragmentModule;
import com.jaredzhang.testapp.ui.module.MainActivityModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by jaredzhang on 23/11/15.
 */

@Singleton
@Component(modules = {
                AppModule.class,
                RealApiModule.class
                }
          )
public interface AppComponent {
    MainActivityComponent plus(MainActivityModule module);
    HotDealsFragmentComponent plus(HotDealsFragmentModule module);
}
