package com.jaredzhang.testapp.data.api;

import android.content.Context;

import com.jaredzhang.testapp.ShopAppApplication;
import com.jaredzhang.testapp.helper.Utils;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import javax.inject.Inject;


import static com.squareup.okhttp.Protocol.HTTP_1_1;


/**
 * Created by jaredzhang on 23/11/15.
 */
final class AuthInterceptor implements Interceptor {

    @Inject
    public AuthInterceptor() {
    }

    @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        // TODO process the request to add the required the authorisation token if needed
        return chain.proceed(request);

    }
}
