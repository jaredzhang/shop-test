package com.jaredzhang.testapp.data.api;


import com.jaredzhang.testapp.SchedulerFactory;
import com.squareup.okhttp.Interceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jaredzhang on 16/11/15.
 */

@Module
public class RealApiModule extends BaseApiModule {

    @Provides
    @Singleton
    public Interceptor providesInterceptor(AuthInterceptor authInterceptor) {
        return authInterceptor;
    }

    @Provides
    @Singleton
    public SchedulerFactory providesSchedulers() {
        return new SchedulerFactory() {
            @Override
            public Scheduler observedOn() {
                return AndroidSchedulers.mainThread();
            }

            @Override
            public Scheduler subscribedOn() {
                return Schedulers.io();
            }
        };
    }
}
