package com.jaredzhang.testapp;

import com.jaredzhang.testapp.data.api.TestApiModule;

/**
 * Created by jaredzhang on 24/11/15.
 */
public class TestShopAppApplication extends ShopAppApplication {
    TestAppComponent mTestAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mTestAppComponent = DaggerTestAppComponent.builder().appModule(new AppModule(this))
                .build();
        setTestComponent(mTestAppComponent);
    }

    public TestAppComponent testComponent() {
        return mTestAppComponent;
    }

}
