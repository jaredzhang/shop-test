package com.jaredzhang.testapp.adapter;

import android.app.Activity;
import android.widget.TextView;

import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.ui.viewmodel.MerchantItemViewModel;

import org.assertj.android.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.util.ActivityController;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jaredzhang on 24/11/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class MerchantListAdapterTest {

    MerchantListAdapter mAdapter;
    ActivityController<Activity> controller;

    @Before
    public void setUp() {
        controller = Robolectric.buildActivity(Activity.class);
        Activity activity = controller.create().get();
        mAdapter = new MerchantListAdapter(RuntimeEnvironment.application);
    }

    @Test
    public void testBindViewHolder() {
        MerchantItemViewModel viewModel = mockMerchantDetail();
        MerchantItemViewModel viewModel2 = mockMerchantDetail();
        mAdapter.addItems(Lists.newArrayList(viewModel, viewModel2));
        assertThat(mAdapter.getItemCount()).isEqualTo(2);

        MerchantListAdapter.ViewHolder viewHolder = mAdapter.onCreateViewHolder(null, 0);
        mAdapter.onBindViewHolder(viewHolder, 0);

        Assertions.assertThat(((TextView) viewHolder.itemView.findViewById(R.id.tv_title))).hasText("title");
        Assertions.assertThat(((TextView) viewHolder.itemView.findViewById(R.id.tv_desc))).hasText("description");
        Assertions.assertThat(((TextView) viewHolder.itemView.findViewById(R.id.tv_exp))).hasText("expiry date");
        Assertions.assertThat(((TextView) viewHolder.itemView.findViewById(R.id.tv_cash_back))).hasText("cashback");

        viewHolder.itemView.findViewById(R.id.btn_shop).performClick();
        verify(viewModel).onShopClick(viewHolder.itemView.findViewById(R.id.btn_shop));

        viewHolder.itemView.performClick();
        verify(viewModel).onItemClick(viewHolder.itemView);

    }

    private MerchantItemViewModel mockMerchantDetail() {
        MerchantItemViewModel viewModel = mock(MerchantItemViewModel.class);
        when(viewModel.getLogoUrl()).thenReturn("http://logo.com");
        when(viewModel.getCategoryLogoUrl()).thenReturn("http://category.com");
        when(viewModel.getTitle()).thenReturn("title");
        when(viewModel.getDescription()).thenReturn("description");
        when(viewModel.getExpiryDate()).thenReturn("expiry date");
        when(viewModel.getCashBack()).thenReturn("cashback");
        return viewModel;
    }

    @After
    public void tearDown() {
        controller.destroy();
    }
}
