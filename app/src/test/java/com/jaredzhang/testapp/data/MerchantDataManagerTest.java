package com.jaredzhang.testapp.data;

import com.jaredzhang.testapp.MockClient;
import com.jaredzhang.testapp.TestShopAppApplication;
import com.jaredzhang.testapp.entity.Merchant;
import com.jaredzhang.testapp.helper.Utils;
import com.jaredzhang.testapp.ui.HotDealsFragment;
import com.jaredzhang.testapp.ui.MainActivity;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import rx.observers.TestSubscriber;


import static com.squareup.okhttp.Protocol.HTTP_1_1;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jaredzhang on 25/11/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class MerchantDataManagerTest {

    @Inject
    MerchantDataManager mMerchantDataManager;
    @Inject
    MockClient mMockClient;

    @Before
    public void setUp() {
        ((TestShopAppApplication) RuntimeEnvironment.application).testComponent().inject(this);
    }

    @Test
    public void testGetData() throws IOException {
        final String json = Utils.readFromAsset(RuntimeEnvironment.application, "mock/get_merchant.json");
        Response.Builder builder = new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).protocol(HTTP_1_1);
        mMockClient.enqueueResponse(builder);
        TestSubscriber<List<Merchant>> testSubscriber = new TestSubscriber<>();
        mMerchantDataManager.getMerchant(1, 20).subscribe(testSubscriber);
        testSubscriber.assertCompleted();
        assertThat(testSubscriber.getOnNextEvents().get(0)).hasSize(20);
    }

    @Test
    public void testGetDataError() throws IOException {
        IOException exception = new IOException("time out");
        mMockClient.enqueueIOException(exception);
        TestSubscriber<List<Merchant>> testSubscriber = new TestSubscriber<>();
        mMerchantDataManager.getMerchant(1, 20).subscribe(testSubscriber);
        testSubscriber.assertError(exception);
    }
}
