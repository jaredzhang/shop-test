package com.jaredzhang.testapp.data.api;


import android.content.Context;

import com.jaredzhang.testapp.MockClient;
import com.jaredzhang.testapp.SchedulerFactory;
import com.squareup.okhttp.Interceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by jaredzhang on 16/11/15.
 */

@Module
public class TestApiModule extends BaseApiModule {

    @Provides
    @Singleton
    public Interceptor providesInterceptor(MockClient mockClient) {
        return mockClient;
    }

    @Provides
    @Singleton
    public MockClient providesMockClient() {
        return new MockClient();
    }

    @Provides
    @Singleton
    public SchedulerFactory providesSchedulers() {
        return new SchedulerFactory() {
            @Override
            public Scheduler observedOn() {
                return Schedulers.immediate();
            }

            @Override
            public Scheduler subscribedOn() {
                return Schedulers.immediate();
            }
        };
    }
}
