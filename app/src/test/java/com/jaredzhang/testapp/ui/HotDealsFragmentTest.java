package com.jaredzhang.testapp.ui;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;

import com.jaredzhang.testapp.MockClient;
import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.TestShopAppApplication;
import com.jaredzhang.testapp.data.MerchantDataManager;
import com.jaredzhang.testapp.helper.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.util.ActivityController;

import java.io.IOException;

import javax.inject.Inject;


import static com.squareup.okhttp.Protocol.HTTP_1_1;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jaredzhang on 24/11/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class HotDealsFragmentTest {

    ActivityController<MainActivity> controller;

    @Inject
    MockClient mMockClient;

    @Inject
    MerchantDataManager mMerchantDataManager;

    MainActivity mainActivity;
    HotDealsFragment mHotDealsFragment;

    @Before
    public void setUp() {
        controller = Robolectric.buildActivity(MainActivity.class);
        mainActivity = controller.create().get();
        ((TestShopAppApplication) RuntimeEnvironment.application).testComponent().inject(this);
    }

    @Test
    public void testSuccessResponse() throws IOException {
        final String json = Utils.readFromAsset(mainActivity,  "mock/get_merchant.json");
        Response.Builder builder = new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).protocol(HTTP_1_1);
        mMockClient.enqueueResponse(builder);

        controller.resume().visible();

        RecyclerView recyclerView = (RecyclerView) mainActivity.findViewById(R.id.recycler_view);
        SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) mainActivity.findViewById(R.id.refresh_view);

        recyclerView.measure(0, 0);
        recyclerView.layout(0, 0, 100, 10000);
        assertThat(recyclerView.getAdapter().getItemCount()).isEqualTo(20);
        assertThat(refreshLayout.isRefreshing()).isFalse();
    }

    @Test
    public void testErrorResponse() throws IOException {
        mMockClient.enqueueUnexpectedException(new RuntimeException());
        controller.resume().visible();
        mHotDealsFragment = (HotDealsFragment) mainActivity.getSupportFragmentManager().findFragmentByTag(HotDealsFragment.class.getName());

        RecyclerView recyclerView = (RecyclerView) mainActivity.findViewById(R.id.recycler_view);
        SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) mainActivity.findViewById(R.id.refresh_view);
        recyclerView.measure(0, 0);
        recyclerView.layout(0, 0, 100, 10000);
        assertThat(recyclerView.getAdapter().getItemCount()).isEqualTo(0);
        assertThat(refreshLayout.isRefreshing()).isFalse();
    }

    @Test
    public void testClickItemView() throws IOException {
        final String json = Utils.readFromAsset(mainActivity,  "mock/get_merchant.json");
        Response.Builder builder = new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).protocol(HTTP_1_1);
        mMockClient.enqueueResponse(builder);
        controller.resume().visible();

        RecyclerView recyclerView = (RecyclerView) mainActivity.findViewById(R.id.recycler_view);
        SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) mainActivity.findViewById(R.id.refresh_view);

        recyclerView.measure(0, 0);
        recyclerView.layout(0, 0, 100, 10000);

        recyclerView.findViewHolderForAdapterPosition(0).itemView.performClick();

        MerchantDetailFragment  merchantDetailFragment = (MerchantDetailFragment) mainActivity.getSupportFragmentManager().findFragmentById(R.id.content_frame);
        assertNotNull(merchantDetailFragment);

    }

    @After
    public void tearDown() {
        controller.pause().stop().destroy();
    }
//

}
