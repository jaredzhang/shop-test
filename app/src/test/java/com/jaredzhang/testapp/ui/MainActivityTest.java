package com.jaredzhang.testapp.ui;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.ShopAppApplication;
import com.jaredzhang.testapp.TestShopAppApplication;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.util.ActivityController;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by jaredzhang on 24/11/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class MainActivityTest {

    ActivityController<MainActivity> controller;
    MainActivity mainActivity;
    NavigationView navigationView;
    DrawerLayout drawerLayout;

    @Before
    public void setUp() {
        controller = Robolectric.buildActivity(MainActivity.class);
        controller.create().resume().visible();
        mainActivity = controller.get();
        navigationView = (NavigationView) mainActivity.findViewById(R.id.nav_view);
        drawerLayout = (DrawerLayout) mainActivity.findViewById(R.id.drawer_layout);
    }

    @Test
    public void testDrawerItems() {
        assertEquals(5, navigationView.getMenu().size());
        assertTrue(navigationView.getMenu().findItem(R.id.nav_featured).isChecked());
        assertThat(mainActivity.getSupportFragmentManager().findFragmentById(R.id.content_frame), instanceOf(FeaturedFragment.class));
    }

    @Test
    public void testDrawerClickSameItem() {
        drawerLayout.openDrawer(GravityCompat.START);
        MenuItem menuItem = mock(MenuItem.class);
        when(menuItem.getItemId()).thenReturn(R.id.nav_featured);
        mainActivity.onNavigationItemSelected(menuItem);
        assertTrue(navigationView.getMenu().findItem(R.id.nav_featured).isChecked());
        assertFalse(drawerLayout.isDrawerOpen(GravityCompat.START));
        assertThat(mainActivity.getSupportFragmentManager().findFragmentById(R.id.content_frame), instanceOf(FeaturedFragment.class));
    }

    @Test
    public void testDrawerClickOtherItems() {
        MenuItem menuItem = mock(MenuItem.class);
        when(menuItem.getItemId()).thenReturn(R.id.nav_search);
        mainActivity.onNavigationItemSelected(menuItem);
        assertThat(mainActivity.getSupportFragmentManager().findFragmentById(R.id.content_frame), instanceOf(CommingSoonFragment.class));
    }


    @After
    public void tearDown() {
        controller.destroy();
    }
}
