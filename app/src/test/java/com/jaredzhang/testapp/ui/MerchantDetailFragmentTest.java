package com.jaredzhang.testapp.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.ui.viewmodel.MerchantItemViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.internal.Shadow;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.util.ActivityController;


import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.assertj.android.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by jaredzhang on 24/11/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class MerchantDetailFragmentTest {

    ActivityController<FragmentActivity> controller;

    @Before
    public void setUp() {
        controller = Robolectric.buildActivity(FragmentActivity.class);
        controller.create();
    }

    @Test
    public void testMerchantDetail() {
        MerchantItemViewModel merchant = mockMerchantDetail();

        Bundle bundle = new Bundle();
        bundle.putParcelable(MerchantDetailFragment.EXTRA_MERCHANT, merchant);
        FragmentActivity activity = controller.get();
        Fragment fragment = Fragment.instantiate(activity, MerchantDetailFragment.class.getName(), bundle);
        activity.getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commit();
        controller.resume().visible();

        assertThat(((TextView) activity.findViewById(R.id.tv_title))).hasText("title");
        assertThat(((TextView) activity.findViewById(R.id.tv_desc))).hasText("description");
        assertThat(((TextView) activity.findViewById(R.id.tv_exp))).hasText("expiry date");
        assertThat(((TextView) activity.findViewById(R.id.tv_cash_back))).hasText("cashback");

        activity.findViewById(R.id.btn_shop).performClick();
        verify(merchant).onShopClick(activity.findViewById(R.id.btn_shop));

        // Click menu
        MenuItem menuItem = mock(MenuItem.class);
        when(menuItem.getItemId()).thenReturn(R.id.action_share);
        fragment.onOptionsItemSelected(menuItem);
        verify(merchant).onShareClick(any(View.class));

    }

    @After
    public void tearDown() {
        controller.pause().stop().destroy();
    }

    private MerchantItemViewModel mockMerchantDetail() {
        MerchantItemViewModel viewModel = mock(MerchantItemViewModel.class);
        when(viewModel.getLogoUrl()).thenReturn("http://logo.com");
        when(viewModel.getCategoryLogoUrl()).thenReturn("http://category.com");
        when(viewModel.getTitle()).thenReturn("title");
        when(viewModel.getDescription()).thenReturn("description");
        when(viewModel.getExpiryDate()).thenReturn("expiry date");
        when(viewModel.getCashBack()).thenReturn("cashback");
        return viewModel;
    }

}
