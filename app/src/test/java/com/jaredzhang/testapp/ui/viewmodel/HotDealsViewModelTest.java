package com.jaredzhang.testapp.ui.viewmodel;

import android.content.Context;

import com.jaredzhang.testapp.MockClient;
import com.jaredzhang.testapp.TestAppComponent;
import com.jaredzhang.testapp.TestShopAppApplication;
import com.jaredzhang.testapp.data.MerchantDataManager;
import com.jaredzhang.testapp.data.api.ApiConfig;
import com.jaredzhang.testapp.entity.Merchant;
import com.jaredzhang.testapp.helper.Utils;
import com.jaredzhang.testapp.ui.MainActivity;
import com.jaredzhang.testapp.ui.module.HotDealsFragmentModule;
import com.jaredzhang.testapp.ui.view.HotDealsView;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.trello.rxlifecycle.RxLifecycle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.observers.TestSubscriber;


import static com.squareup.okhttp.Protocol.HTTP_1_1;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by jaredzhang on 25/11/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class HotDealsViewModelTest {

    @Inject
    MockClient mMockClient;
    @Inject
    MerchantDataManager mMerchantDataManager;

    HotDealsView mHotDealsView;
    HotDealsViewModel mViewModel;

    @Before
    public void setUp() {
        mHotDealsView = Mockito.mock(HotDealsView.class);
        TestAppComponent testAppComponent = ((TestShopAppApplication) RuntimeEnvironment.application).testComponent();
        testAppComponent.inject(this);
        mViewModel = new HotDealsViewModel(RuntimeEnvironment.application, mMerchantDataManager, mHotDealsView);
    }

    @Test
    public void testFetch() throws IOException {
        final String json = Utils.readFromAsset(RuntimeEnvironment.application, "mock/get_merchant.json");
        Response.Builder builder = new Response.Builder().body(ResponseBody.create(MediaType.parse("application/json"), json)).code(200).protocol(HTTP_1_1);
        mMockClient.enqueueResponse(builder);
        TestSubscriber<List<MerchantItemViewModel>> testSubscriber = new TestSubscriber<>();
        mViewModel.fetchData(1).subscribe(testSubscriber);
        testSubscriber.assertCompleted();
        assertThat(testSubscriber.getOnNextEvents().get(0)).hasSize(20);
    }

    @Test
    public void testFetchError() throws IOException {
        IOException exception = new IOException("time out");
        mMockClient.enqueueIOException(exception);
        TestSubscriber<List<MerchantItemViewModel>> testSubscriber = new TestSubscriber<>();
        mViewModel.fetchData(1).subscribe(testSubscriber);
        testSubscriber.assertError(exception);
    }
}
