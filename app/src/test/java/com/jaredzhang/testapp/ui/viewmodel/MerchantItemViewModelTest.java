package com.jaredzhang.testapp.ui.viewmodel;

import com.jaredzhang.testapp.R;
import com.jaredzhang.testapp.data.api.JSONParser;
import com.jaredzhang.testapp.entity.Merchant;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import static junit.framework.Assert.assertEquals;

/**
 * Created by jaredzhang on 24/11/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class MerchantItemViewModelTest {

    private static final String MERCHANT_JSON = "{\n" +
            "    \"id\": 1,\n" +
            "    \"name\": \"Izio\",\n" +
            "    \"description\": \"sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui\",\n" +
            "    \"cashback\": 74,\n" +
            "    \"logoUrl\": \"http://placehold.it/100x50\",\n" +
            "    \"expiryDate\": \"11/28/2014\",\n" +
            "    \"category\": \"SELZENTRY\",\n" +
            "    \"categoryLogo\": \"http://placehold.it/50x50\",\n" +
            "    \"merchantUrl\": \"https://cam.ac.uk/maecenas/ut/massa/quis.html?nibh=vitae&quisque=nisi\"\n" +
            "  }";

    private static final String MERCHANT_NULL_EXPIRY_JSON = "{\n" +
            "    \"id\": 1,\n" +
            "    \"name\": \"Izio\",\n" +
            "    \"description\": \"sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui\",\n" +
            "    \"cashback\": 74,\n" +
            "    \"logoUrl\": \"http://placehold.it/100x50\",\n" +
            "    \"category\": \"SELZENTRY\",\n" +
            "    \"categoryLogo\": \"http://placehold.it/50x50\",\n" +
            "    \"merchantUrl\": \"https://cam.ac.uk/maecenas/ut/massa/quis.html?nibh=vitae&quisque=nisi\"\n" +
            "  }";

    @Test
    public void testMapExpiryDateFromMerchant() {
        Merchant merchant = JSONParser.parseFromString(Merchant.class, MERCHANT_JSON);
        MerchantItemViewModel merchantItemViewModel = MerchantItemViewModel.mapFrom(RuntimeEnvironment.application, merchant);
        assertEquals(RuntimeEnvironment.application.getString(R.string.exp, merchant.getExpiryDate()), merchantItemViewModel.expiryDate);
    }

    @Test
    public void testMapExpiryDateNullFromMerchant() {
        Merchant merchant = JSONParser.parseFromString(Merchant.class, MERCHANT_NULL_EXPIRY_JSON);
        MerchantItemViewModel merchantItemViewModel = MerchantItemViewModel.mapFrom(RuntimeEnvironment.application, merchant);
        assertEquals(RuntimeEnvironment.application.getString(R.string.never_expires), merchantItemViewModel.expiryDate);
    }
}
